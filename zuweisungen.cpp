#include <iostream>
#include <limits>


int main() {
    /* this main contain some limits of the computer */
    std::cout << "Grenzwerte für Ganzzahl-Typen "  << std::endl;
    std::cout << "int Minimum =         " << std::numeric_limits<int>::min() << '\n';
    std::cout << "int Maximum =         " << std::numeric_limits<int>::max() << '\n';
    std::cout << "long Minimum =        " << std::numeric_limits<long>::min() << '\n';
    std::cout << "int Maximium =        " << std::numeric_limits<long>::max() << '\n';

    int m {9};
    std::cout << "m = "  << m << '\n';
    return 0;
}
